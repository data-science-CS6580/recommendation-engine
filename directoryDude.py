import os
import re

regExpression = "^([a-z]|[A-Z]|\s)*:$" #expression to find 

#thank you to: https://www.geeksforgeeks.org/how-to-iterate-over-files-in-directory-using-python/
def listAllFilesInDirectory(directory="recipes"):
    files = []
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)

        #filter out items in directory that are not files
        if os.path.isfile(f):
            files.append(f)
    
    return files


def file2Dic(filePath):
    """
    iterates over a file and if the line its looking at is like "HEADER:"
    make it the key in a dictionary and the lines below it become part of the value

    returns: dictionary 
    """
    dictObject = {}

    stringToBeStored = ""
    key=None

    File = open(filePath, 'r').readlines()
    for line in File:
        line = line.strip()

        if re.search(regExpression, line):
            if key != None: 

                if stringToBeStored == "":
                    stringToBeStored = "0"

                dictObject[key] = stringToBeStored

            key = line.removesuffix(":")
            stringToBeStored = ""
        else:
            stringToBeStored += line
    
    return dictObject

def reviewFile2Dic(filePath):
    returnList = []

    dictObject = {}

    stringToBeStored = ""
    key=None

    File = open(filePath, 'r').readlines()
    for line in File:
        line = line.strip()

        if re.search(regExpression, line):

            if key != None: 

                if stringToBeStored == "":
                    stringToBeStored = "0"

                if "dateLastModified" in line:
                    returnList.append(dictObject.copy())
                    dictObject = {}

                dictObject[key] = stringToBeStored

            key = line.removesuffix(":")
            stringToBeStored = ""
        else:
            stringToBeStored += line
    
    return returnList
