import sqlite3
from sqlite3 import Error
from directoryDude import listAllFilesInDirectory, file2Dic, reviewFile2Dic

#info courtesy of https://www.sqlitetutorial.net/sqlite-python/sqlite-python-select/



def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def create_curser(db_file="./recipes.db"):
    conn = create_connection(db_file)
    return conn.cursor()


def loadRecipes(cur):
    recipeFiles = listAllFilesInDirectory("./recipes")
    for filez in recipeFiles:
        parcedFile = file2Dic(filez)

        if parcedFile == {}:
            continue

        recipeID = filez.removeprefix('./recipes/')
        recipeID = recipeID.removesuffix('.txt')
        if recipeID.isnumeric() == False:
            continue

        numRatings = parcedFile['Number of ratings'].removesuffix(" Ratings").replace(',','').removesuffix(" Rating")
        parcedFile["Title"] = parcedFile["Title"].replace("'",'').removesuffix(" | Allrecipe")
        parcedFile["Submitter"] = parcedFile["Submitter"].replace("'", "")

        if numRatings == "NA":
            numRatings = 0
        if parcedFile["Number of stars"] == 'Unrated' or parcedFile["Number of stars"] == 'NA':
            parcedFile["Number of stars"] = "0.00"
        #try:
        sqlquery = f"insert into Recipes (id, url, title, image, submitter, numRatings, numStars)VALUES({recipeID}, '{parcedFile['URL']}', '{parcedFile['Title']}', '{parcedFile['Image']}', '{parcedFile['Submitter']}', {numRatings}, {round(float(parcedFile['Number of stars']),2)});"
        try:
            cur.execute(sqlquery)
        except:
            print(sqlquery)
            exit(1)

def loadReviews(cur):
    currentReviewID = 1

    reviewFiles = listAllFilesInDirectory("./recipe_reviews")
    for filez in reviewFiles:
        parsedFile = reviewFile2Dic(filez)

        if isinstance(parsedFile, list) == False or len(parsedFile) <= 1:
            continue

        recipeNumber = parsedFile[0]["Recipe number"].removesuffix('.txt')
        if recipeNumber.isnumeric() == False:
            continue

        for i, review in enumerate(parsedFile):
            if i == 0 or review['userID'].isnumeric() == False:
                continue


            sqlquery = f"insert into reviews (id, userID, recipeID, rating)VALUES({currentReviewID}, {review['userID']} ,{recipeNumber}, {review['Rating']});"
            currentReviewID += 1
            try:
                cur.execute(sqlquery)
            except:
                print("\033[31m\u001b[1mProblem with querry: \u001b[0m\033[31m" + sqlquery+ "\u001b[0m")
                exit(1)


def main():
    database = r"./recipes.db"
    restartDB = True
    

    # create a database connection
    conn = create_connection(database)
    with conn:
        cur = conn.cursor()
        if restartDB:
            cur.execute("DROP TABLE IF EXISTS Recipes")
            cur.execute("DROP TABLE IF EXISTS Reviews")
        cur.execute("CREATE TABLE Recipes (id INT, url CHAR, title CHAR, image CHAR, submitter CHAR, numRatings INT, numStars REAL)")
        cur.execute("CREATE TABLE Reviews (id INT, userID INT, recipeID INT, rating REAL)")

        print("Inserting recipes...")
        loadRecipes(cur)
        cur.execute("SELECT COUNT(*) FROM recipes;")
        print(f"rows effected: {cur.fetchall()[0][0]}\n")

        print("Inserting reviews...")
        loadReviews(cur)
        cur.execute("SELECT COUNT(*) FROM reviews;")
        print(f"rows effected: {cur.fetchall()[0][0]}")



if __name__ == '__main__':
    main()