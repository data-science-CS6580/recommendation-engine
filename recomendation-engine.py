import os
from loaddb import create_curser, main

def userInputToSearchableString(userString):
    userString = "%"+userString+"%"
    return userString.replace(" ", "%")

def printandexecute(query):
    cur.execute(query)
    x = cur.fetchall()
    print(x)
    return x

def searchandPrettyPrint(query):
    cur.execute(query)
    results = cur.fetchall()
    print()
    for result in results:
        print(f"{result[0]} | \033[95m\033[1m{result[1]}\033[0m by {result[2]}")

def searchRecipes(userString, numberLimit=10, filterPopular=False):
    userString = userInputToSearchableString(userString)
    query = f"SELECT id, title, submitter FROM recipes WHERE title LIKE '{userString}'  ORDER BY numRatings desc LIMIT {numberLimit};"
    if filterPopular:
        query = f"SELECT id, title, submitter FROM recipes WHERE title LIKE '{userString}' AND numRatings < 100 ORDER BY numRatings desc LIMIT {numberLimit};"

    searchandPrettyPrint(query)


def mainMenu():
    print("\u001b\n[32mWelcome to Daniel's Recomendation Engine\u001b[0m")
    print("----------------------------------------")
    print("1  | Search Database")
    print("2  | Search 'Chicken Tacos'")
    print("3  | Get Recomendations")
    print("0  | Exit")
    x = int(input("Enter number: "))
    if x == 0:
        exit(0)
    return x

def getRecomendations():
    likedRecipes = []
    
    while True:
        numSearchResults = 10
        inputText = "\nSearch Recipe Titles: " 
        if len(likedRecipes) > 5:
            inputText = "\nSearch Recipe Titles (R for recomendations): "

        userSearch = input(inputText)
        if userSearch == 'R' or userSearch == 'r':
            break
        searchRecipes(userSearch, numSearchResults)


        print("C     | Clear search")
        print("F     | Filter out most popular")
        print("S     | Show more")

        userPicked = input("Enter recipe id number: ")
        if userPicked == 'F' or userPicked == 'f':
            searchRecipes(userSearch, numSearchResults, True)
            userPicked = input("Enter recipe id number: ")
            
        if userPicked == 's' or userPicked == 'S':
            if numSearchResults <= 40:
                numSearchResults += 10
            searchRecipes(userSearch, numSearchResults)
            userPicked = input("Enter recipe id number: ")

        if userPicked != 'C' and userPicked != 'c' and userPicked != 'S' and userPicked != 's':
            try:
                likedRecipes.append(int(userPicked))
            except:
                print("\033[91mNot a number!\033[0m")
                userPicked = input("Enter recipe id number: ")
                likedRecipes.append(int(userPicked))


    print()
    sqlQuery = f"SELECT userID, count(*) as helpfulreviews FROM reviews WHERE recipeID ="
    for i, id in enumerate(likedRecipes):
        sqlQuery += f"{id}"
        if len(likedRecipes)-1 > i:
            sqlQuery += " OR recipeID = "
        else:
            sqlQuery += " GROUP BY userID ORDER BY helpfulreviews desc LIMIT 10;"
    results = printandexecute(sqlQuery)


    sqlQuery = f"SELECT recipeID, count(*) as helpfulUser FROM reviews WHERE (userID = "
    for i, id in enumerate(results):
        sqlQuery += f"{id[0]}"
        if len(results)-1 > i:
            sqlQuery += " OR userID = "
        else:
            sqlQuery += ") AND  userID <> " #GROUP BY recipeID ORDER BY helpfulUser desc LIMIT 10;"
            for z, x in enumerate(likedRecipes):
                sqlQuery += f"{x}"
                if len(likedRecipes)-1 > z:
                    sqlQuery += " and userID <> "
                else:
                    sqlQuery += " GROUP BY recipeID ORDER BY helpfulUser desc LIMIT 10;"

            print()
    results = printandexecute(sqlQuery)

    print("\033[36m\n\033[1mRecomendations:\033[0m")
    sqlQuery = f"SELECT id, title, submitter FROM recipes WHERE id = "
    for i, id in enumerate(results):
        sqlQuery += f"{id[0]}"
        if len(results)-1 > i:
            sqlQuery += " OR id = "
        else:
            sqlQuery += " LIMIT 15;"
    searchandPrettyPrint(sqlQuery)

if __name__ == '__main__':
    database = r"./recipes.db"

    if os.path.exists(database) == False:
        if os.path.exists(r"./recipes") and os.path.exists(r"./recipe_reviews"):
            print("No database found. Generating new one")
            main()
        else:
            print("\033[31mNo db or recipe files found\u001b[0m")
            exit(1)

    cur = create_curser(database)

    while True:
        mm = mainMenu()
        if mm == 1:
            userSearch = input("Search Recipe Titles: ")
            searchRecipes(userSearch)

        if mm == 2:
            searchandPrettyPrint(f"SELECT id, title, submitter, numRatings FROM recipes WHERE title LIKE '%chicken%tacos%'  ORDER BY numRatings desc LIMIT 10;")
        
        if mm == 3:
            getRecomendations()

