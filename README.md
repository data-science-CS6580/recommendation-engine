# Recommendation Engine

### To Run
```sh
python3 recomendation-engine.py
```
The above command will work if the recipes.db file is present, if not but you have the recipes and recipe_review folders then you it will still work but it will take a bit longer the first time to run because it as to create the db.


### Init DB
```sh
python3 loaddb.py
```
Run this command first if you don't have the db initialized yet. You will need the recipes/ and and recipe_reviews/ folders in the repo. This script is automatacly ran in the recomendation-engine if you dont care to set it up a head of time.